Vim plugin for coloring and auto-indenting a Makefile without tabs.

Example:
```
ifeq ($(origin .RECIPEPREFIX), undefined)
  $(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
endif
.RECIPEPREFIX = >

target:	sources
>	actions

# vim: expandtab: sw=4 ts=4 ft=yrmake:
```

Installation under [vim-plug][vim-plug]:
```
Plug 'https://gitlab.com/yellowrabbit/yrmake-vim-syntax-plugin.git'
```

[vim-plug]:https://github.com/junegunn/vim-plug

